/*create by walter*/
var width = 1000;
var height = 700;

var diagram1= d3.selectAll(".diagram-1")
var svg = diagram1.append("svg")
    .attr("width",width)
    .attr("height",height)
    .style("background-color","rgb(245,248,253)")
/*数据同步加载*/
function dataLoad(filename)  {
    var arraydata;
    $.ajax({
        type: "GET",
        url: filename,
        dataType: "json",
        async: false,
        success: function(json) {arraydata = eval(json) }
    });
    return arraydata;
}

var data = dataLoad("jsonFile/data1.json");

console.log(data);

/*数据更具函数群号分流*/
var lineForm1 =  new Array();

var lineForm2 =  new Array();

     for(z=0;z<data.length;z++) {
         var ar = data[z];
         if (ar.groupName == "A") {
             console.log(ar.groupName)
             lineForm1.push(ar)
         }

         if (ar.groupName == "B") {
             console.log(ar.groupName)
             lineForm2.push(ar)
         }

     };

data.forEach(function (d) {
    d.time = new Date(d.time);
})
/*绘图操作*/

var xMax = d3.max(data,function (d) {
    return d.time;
})
var xMin = d3.min(data,function (d) {
    return d.time;
})
var yMax = d3.max(data,function (d) {
    return d.Active;
})
var yMin = d3.min(data,function (d) {
    return d.Active;
})

var date1 = data[1].time;

console.log("orin-date"+date1)
console.log("output-date"+new Date(date1))
console.log("out2"+new Date(2016,01,01))
console.log("Max"+xMax)
console.log("d3time"+d3.extent(lineForm1,function (d) {
    return d.time;
}))

console.log("d3timeMap"+lineForm1.map(function (d) {
        return d.time;
    }))
// var xScale = d3.scale.ordinal()
//     .domain(lineForm1.map(function (d) {
//         return d.time;
//     }))
//     .rangeRoundBands([0, width])

var xScale = d3.time.scale()
    .domain([new Date(2016,4,6),xMax])
    .range([0,600])
var yScale = d3.scale.linear()
    .domain([yMax,0])
    .range([0,500])
var line = d3.svg.line()
    .x(function (d) {
        return xScale(d.time)
    })
    .y(function (d) {
        return yScale(d.Active)
    })

var xAxis = d3.svg.axis()
    .scale(xScale)
    .tickFormat(d3.time.format("%Y /%m /%d"))
    .orient("bottom")
    // .ticks(10)
    // .transition()
    // .duration(1000)
    // .ticks(6)
var yAxis = d3.svg.axis()
    .scale(yScale)
    .orient("left")

var xline = d3.svg.axis()
    .scale(xScale)
    .tickSize(500,0,0)
    .orient("bottom")
var yline = d3.svg.axis()
    .scale(yScale)
    .tickSize(900,0,0)
    .orient("left")
var xgrid = svg.append("g")
    .attr("class","grid")
    .attr("transform","translate(30,20)")
    .call(xline)
    .selectAll("text")
    .text("")

var ygrid = svg.append("g")
    .attr("class","grid")
    .attr("transform","translate(920,20)")
    .call(yline)
    .selectAll("text")
    .text("")
var gxAxis = svg.append("g")
    .attr("class","xaxis")
    .attr("transform","translate(30,520)")
    .call(xAxis)
    .append("text")
    .text("Time")
    .attr("transform","translate(920,0)")

d3.selectAll("g.tick text")
    .attr('transform','translate(30,20)rotate(30)')
var gyAxis = svg.append("g")
    .attr("class","yaxis")
    .attr("transform","translate(30,20)")
    .call(yAxis)
    .append("text")
    .text("Active")
    .attr("transform","translate(10,-5)")


var line = d3.svg.line()
    .x(function (d) {
        return xScale(d.time)
    })
    .y(function (d) {
        return yScale(d.Active)
    })


//
// var path = svg.append("path")
//     .attr("class","diagram-path")
//     .attr("d",line(lineForm1))
//     .attr("transform","translate(30,20)")
var pathCarvas = svg.append("g")
    .style("margin-left",30+"px")
        pathCarvas.append("path")
            .attr("class", "diagram-path")
            .attr("d", line(lineForm2))
            .attr("transform", "translate(30,20)")

// var path = svg.append("path")
//     .attr("class","diagram-path")
//     .attr("d",line(lineForm2))
//     .attr("transform","translate(30,20)")

 var  count = 0;

var diagram1 = function () {
console.log("countA"+count);
    if(count%2==0) {
        var path = svg.append("path")
            .attr("class", "diagram-path")
            .style("display","block")
            .attr("d", line(lineForm2))
            .attr("transform", "translate(30,20)")
        svg.selectAll(".data-point")
            .data(data)
            .enter()
            .append("g")
            .append("circle")
            .attr("class","data-point")
            .attr("cx",line.x())
            .attr("cy",line.y())
            .attr("r",5.5)
            .attr("transform","translate(30,20)")
            .on("mouseover",function (d) {
                d3.select(this).transition()
                    .duration(500)
                    .attr("r",10)


                tips.html(d.Active+"/人数"+d.time+"/天次")
                    .style("display","block")
                    .style("left",(d3.event.pageX)+"px")
                    .style("top",(d3.event.pageY+20)+"px")
                    .style("opacity",1.0)
                    .style("background","white")

            })
            .on("mouseout",function () {
                d3.select(this).transition()
                    .duration(500)
                    .attr("r",5.5)
                tips.style("display","none")
            })

        count++;
    }
    else {
        svg.selectAll(".diagram-path")
            .style("display","none")
        count++;
    }

    console.log("count"+count);

}

var flag = true;

var period1 = function () {
    if(flag) {
        xScale1 = d3.time.scale()
            .domain([new Date(2016, 4, 6), new Date(2016, 4, 10)])
            .range([0, 600])

        var line = d3.svg.line()
            .x(function (d) {
                return xScale1(d.time)
            })
            .y(function (d) {
                return yScale(d.Active)
            })
        var xAxis = d3.svg.axis()
            .scale(xScale1)
            .tickFormat(d3.time.format("%Y /%m /%d"))
            .orient("bottom")
            .ticks(4)
        console.log("1111")
        d3.selectAll(".diagram-path")
            .transition()
            .duration(1000)
            .attr("d", line(lineForm2))
            .attr("transform", "translate(30,20)");
        flag = false;
    }
    else {
        var data = lineForm2;
        var xScale1 = d3.time.scale()
            .domain([new Date(2016,4,6),xMax])
            .range([0,600])
        var xAxis = d3.svg.axis()
            .scale(xScale1)
            .tickFormat(d3.time.format("%Y /%m /%d"))
            .orient("bottom")

        var line = d3.svg.line()
            .x(function (d) {
                return xScale1(d.time)
            })
            .y(function (d) {
                return yScale(d.Active)
            })
        d3.selectAll(".diagram-path")
            .transition()
            .duration(1000)
            .attr("d", line(lineForm2))
            .attr("transform", "translate(30,20)");
        flag = true;
    }
    d3.selectAll(".xaxis")
        .transition()
        .duration(1000)
        .attr("class", "xaxis")
        .attr("transform", "translate(30,520)")
        .call(xAxis)
    d3.selectAll("g.xaxis g.tick text")
        .attr('transform','translate(30,20)rotate(30)')
        
}






